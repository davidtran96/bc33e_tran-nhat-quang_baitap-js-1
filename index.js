const { log } = require("console");

/**
 * Bài Tập 1
 * Tính tiền lương 1 tháng
 * Lương 1 ngày = 100.000
 * Số ngày làm 1 tháng: 24
 */
var salary1Day = 100000;
var workingDays = 24;
var salary1Month = salary1Day * workingDays;
console.log("Tiền lương 1 tháng: ", salary1Month);

/**
 * Bài tập 2
 * Nhập vào 5 số thực
 * num 1 = 3
 * num 2 = 7
 * num 3 = 10
 * num 4 = 15
 * num 5 = 20
 */
var num1, num2, num3, num4, num5;
num1 = 3;
num2 = 7;
num3 = 10;
num4 = 15;
num5 = 20;
var sum = num1 + num2 + num3 + num4 + num5;
var average = sum / 5;
console.log("Trung bình 5 số thực: ", average);

/**
 * Bài tập 3
 * 1 usd = 23.5000 vnd
 * Nhập vào usd : 20
 */
var usdTovnd = 23500;
var numOfusd = 20;
var totalvnd = numOfusd * usdTovnd;
console.log("Số tiền đổi sang VND: ", totalvnd);

/**
 * Bài tập 4 tính diện tích và chu vi hình chữ nhật
 * Diện tích bằng a * b
 * Chu vi bằng (a + b) * 2
 */

var chieuDai = 5;
var chieuRong = 8;
var dientich = chieuDai * chieuRong;
console.log("Diện tích hình chữ nhật: ", dientich);
var chuvi = (chieuDai + chieuRong) * 2;
console.log("Chu vi hình chữ nhật: ", chuvi);

/**
 * Bài tập 5: Tính tổng 2 ký số
 * Nhập vào số nguyên có 2 chữ số
 * Tính tổng của 2 chữ số
 */

var soNguyen = 96;
var soThuNhat = soNguyen / 10;
var soThuHai = soNguyen % 10;
var tongHaiKySo = soThuNhat + soThuHai;
console.log("Tổng 2 ký số: ", tongHaiKySo);
